prime_sender
=====

Make 2 Genservers such that:

One of them checks every second if the current second is prime. 
In that case it should tell the another genserver to print it to the console.

