-module(prime_printer).

-behaviour(gen_server).

-export([start_link/0, print/1]).

-export([init/1, handle_cast/2, handle_call/3]).

%% Public Functions
start_link() ->
  {ok, Pid} = gen_server:start_link(?MODULE, [], []),
  register(prime_printer, Pid),
  {ok, Pid}.

print(Prime) ->
  gen_server:cast(?MODULE, {print_prime, Prime}).


%% Callbacks
init([]) ->
  {ok, []}.

handle_cast({print_prime, Prime}, State) ->
  io:format("Prime Received: ~p~n", [Prime]),
  {noreply, State}.

handle_call(_Msg, _From, State) ->
  {reply, State, State}.
