-module(prime_sender_app).

-behaviour(application).

-export([start/2, stop/1]).

start(_StartType, _StartArgs) ->
    prime_sender_sup:start_link().

stop(_State) ->
    ok.
