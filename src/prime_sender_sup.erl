-module(prime_sender_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

init([]) ->
    SupFlags = #{strategy => one_for_one, intensity => 1, period => 5},
    % PrimeWatcherSpec = #{id => prime_watcher, start => {prime_watcher, start_link, []}},
    % PrimePrinterSpec = #{id => prime_printer, start => {prime_printer, start_link, []}},
    {ok, {SupFlags, []}}.
