-module(prime_watcher).

-behaviour(gen_server).

-export([start_link/0]).

-export([init/1, handle_info/2, handle_call/3, handle_cast/2, is_prime/1]).

-define(CHECK_INTERVAL, 1000).

%% Public Functions

start_link() ->
  {ok, Pid} = gen_server:start_link(?MODULE, [], []),
  register(prime_watcher, Pid),
  {ok, Pid}.

%% Callbacks

init([]) ->
  erlang:send_after(0, self(), check_sec),
  {ok, []}.

handle_info(check_sec, State) ->
  Sec = os:system_time(seconds),
  case is_prime(Sec) of
    true ->
      prime_printer:print(Sec),
      erlang:send_after(?CHECK_INTERVAL, self(), check_sec);
    false ->
      erlang:send_after(?CHECK_INTERVAL, self(), check_sec)
    end,
  {noreply, State}.

handle_cast(_Msg, State) ->
  {noreply, State}.

handle_call(_Msg, _From, State) ->
  {reply, State, State}.

is_prime(2) ->
  true;
is_prime(N) ->
  Fun = fun(X, Acc) -> Acc andalso N rem X =/= 0 end,
  lists:foldl(Fun, true, lists:seq(2, N-1)).
